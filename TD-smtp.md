# TD postfix

## Environnement

Utilisez le Vagrantfile dans le dossier compressé [tp-smtp.tgz](./files/tp-smtp.tgz) afin de créer l’architecture powerdns + postfix

![tp-smtp](./images/tp-smtp.png)

Nous avons :

- Deux serveurs DNS de récurssion accessible que depuis le réseaux interne (192.168.33.0/24)
- Deux serveur autoritatif disposant d'une interface web d’administration accessible sur le reseaux interne.
- Le domaine lab.local est povisionné et les serveurs récurssifs transmettent les requêtes pour ce domaine vers le serveur autoritatif.
- Le serveur smtp est configurer afin de disposer de domaine virtuel dans une base de données mysql. avec Dovecote qui utilise la même base pour les comptes.

L’interface web PowerAdmin (dns) est disponible sur les deux serveurs autoritatif via les urls :  http://192.168.33.31/poweradmin/ et http://192.168.33.32/poweradmin/ 
Login : admin ; mot de passe xxxxxx

L’interface web PostfixAdmin est disponible sur le serveur smtp via l’urls :
http://192.168.33.41/postfixadmin
Login : hostmaster@lab.local ; mot de passe : zzzxxx00

## Consultation

Consultez les interfaces web poweradmin et postfix admin. Et regardez ce qui à été fait pour le domaine lab.local.

- Connectez vous au host recursor-1
  - consulter la configuration faite sur le fichier /etc/pdns-recursor/recursor.conf :

    ```bash
    diff /etc/pdns-recursor/recursor.conf /etc/pdns-recursor/recursor.conf.orig
    ```

  - Consultez la configuration du resolver et interroger le domaine lab.local avec dig

    ```bash
    cat /etc/resolv.conf
    .../...
    dig lab.local +short
    .../...
    dig MX lab.local +short
    ```

- Connectez vous au host auth-1
  - consulter la configuration pdns

    ```bash
    diff /etc/pdns/pdns.conf /etc/pdns/pdns.conf.orig
    ```

  - envoyez un mail avec mailx (le mail se termine avec une ligne ne contenant qu'un point)

    ```bash
    [vagrant@auth-1 ~]$ mailx -s testing-email test@lab.local
    this is just a test

    .
    EOT
    [vagrant@auth-1 ~]$
    ```

- Connectez vous au host smtp
  - consultez la configuration postfix :

   ```bash
   [root@smtp ~]# diff /etc/postfix/main.cf /etc/postfix/main.cf.orig
   .../...
   ```

  - consultez le mail précédament envoyé

  ```bash
  [root@smtp ~]# cat /var/vmail/lab.local/test@lab.local/new/1587066535.M367838P6734.smtp.lab.local\,S\=840\,W\=862
  Return-Path: <vagrant@auth-1.localdomain>
  Delivered-To: test@lab.local
  Received: from smtp.lab.local
    by smtp.lab.local with LMTP id OA8GEqe2mF5OGgAAKOplyA
    for <test@lab.local>; Thu, 16 Apr 2020 19:48:55 +0000
  Received: from auth-1.localdomain (unknown [192.168.56.31])
    by smtp.lab.local (Postfix) with ESMTP id 30321499AED
    for <test@lab.local>; Thu, 16 Apr 2020 19:48:55 +0000 (UTC)
  Received: by localhost.localdomain (Postfix, from userid 1000)
    id C422664961; Thu, 16 Apr 2020 19:48:54 +0000 (UTC)
  Date: Thu, 16 Apr 2020 19:48:54 +0000
  To: test@lab.local
  Subject: testing-email
  User-Agent: Heirloom mailx 12.5 7/5/10
  MIME-Version: 1.0
  Content-Type: text/plain; charset=us-ascii
  Content-Transfer-Encoding: 7bit
  Message-Id: <20200416194854.C422664961@auth-1.localdomain>
  From: vagrant@auth-1.localdomain (vagrant)
  
  this is just a test
  
  [root@smtp ~]#
  ```

  Qui est l'éméteur de ce mail ?
  En utilisant le [TD](./TD-mail-system.md) de configuration de l'envois smtp. Configurez auth-1 afin que ce mail soit envoyé avec l'identité vagrant@auth-1.lab.local s'il est envoyé de la même façon.
  Expliquez moi la configuration réalisée.

## Actions à faire

- Créez un compte mail sur le domain lab.local.

- Où se situe la mailbox pour ce compte ? Comment l’avez vous trouvée ?

- Créez un nouveau domaine de messagerie sur cet environement , décrivez vos actions sur les serveurs dns recurseur, authoritatif et sur postfixadmin

- Comment validez-vous le bon fonctionnement de ce domaine au travers de smtp et dovecot : Décrivez vos actions permettant d’effectuer cette validation.

- Où sont enregistrés les comptes de messageries ?

- Quels sont les user et groupes propriétaires des boites de messagerie ? Comment cela est-t-il parametré ?

- Comment réaliser la sauvegarde des boites de messageries ?

## Pour s'amuser

- lancez le provisionning "hey"

  ```bash
  $ vagrant provision --provision-with hey
  ...
  ```

  Voilà! les utilisateur se plaignent de ne plus avoir internet, reparrez maintenant.

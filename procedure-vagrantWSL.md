# Installer correctement Vagrant avec WSL

Dans votre subsystem (ici Ubuntu) vous allez devoir mettre dans votre bashrc ces 3 lignes :

```bash
export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"
export VAGRANT_WSL_WINDOWS_ACCESS_USER_HOME_PATH="/mnt/c/Users/VotreUserWindows"

export PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox"
```

Puis, mettez ceci dans `/etc/wsl.conf`

```
[automount]
options = "metadata,umask=22,fmask=11"
```

Ensuite dans un CLI Windows :

```powershell
wsl --terminate ubuntu
wsl
```

Vous en avez fini, normalement tout devrait fonctionner avec Vagrant.
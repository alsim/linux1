# systemd


<!-- vim-markdown-toc GitLab -->

* [Présentation](#présentation)
    * [Rapidité du démarrage](#rapidité-du-démarrage)
    * [Organisation plus souple](#organisation-plus-souple)
    * [Complétude](#complétude)
    * [Mais encore](#mais-encore)
        * [Daemons](#daemons)
        * [Fichiers](#fichiers)
    * [Outils de gestion système propres à chaque distribution](#outils-de-gestion-système-propres-à-chaque-distribution)
* [Utilisation](#utilisation)
    * [`systemctl`](#systemctl)
* [Configuration `systemd`](#configuration-systemd)
    * [Les composants](#les-composants)
    * [Les *unités* et *targets*](#les-unités-et-targets)
    * [Configuration génériques des unités](#configuration-génériques-des-unités)
        * [Les sections](#les-sections)
        * [Les tokens de configuration des unités](#les-tokens-de-configuration-des-unités)
        * [Commande pratiques de gestion des spécifications *d'unités*](#commande-pratiques-de-gestion-des-spécifications-dunités)
    * [Les *unités* de type *target*](#les-unités-de-type-target)
    * [*Unité* de type *service*](#unité-de-type-service)
    * [*Unité* conditionnant l'activation d'un *service*](#unité-conditionnant-lactivation-dun-service)
        * [*Unité* de type *socket*](#unité-de-type-socket)
        * [Unité de type *timer*](#unité-de-type-timer)
        * [*Unité* de type *path*](#unité-de-type-path)

<!-- vim-markdown-toc -->

## Présentation

`systemd` a pour principale fonction d'être le **processus init** des distributions GNU/Linux ; le processus de **PID 1**. Tout comme les autres versions d'init, au démarrage du système, il effectue les opérations d'initialisation du système comme démarrer les daemons ou monter les systèmes de fichier.

Les autres version d'init :

- Init systemV ou BSD (historique UNIX)
- GNU/dnd (init du projet GNU/hurd, il est peu utilisé)
- upstart (version précédant `systemd` proposant aussi de la parallélisation au démarrage)
- Initng (il était le concurent direct de `systemd`)
- launchd (init utilisé sur MacOS, `systemd` y a tiré beaucoup de son inspiration)
- bien d'autres tentatives anecdotiques du processus init (pinit, finit, speedboot ... )

Bref, `systemd` de part sa simplicité, sa complétude, sa modularité, et la normalisation qu'il propose est devenu la version contemporaine d'**init** de référence.

### Rapidité du démarrage

`systemd` permet de réduire de façon importante le temps de démarrage du système car il ordonnance de façon précise l'exécution des opérations de démarrage en parallélisant un maximum d'entre elles.

Notamment, la résolution des dépendances entre les services est grandement accélérée par :

- La création de l'ensemble des sockets de communication pour les daemons avant de lancer les daemons eux-mêmes.
- L'attente de l'obtention du nom **D-Bus** pour passer au service suivant, plutôt que d'attendre la fin du démarrage complet du service.

Un *service* n'attend plus la fin du démarrage des *services* dont il dépend car il dispose d'un accès en avance de phase de leurs socket et de leurs noms dbus.

La commande suivante fournie une analyse de la séquence du boot dans une image *(plot)* ou un résumé du chemin critique textuel :

```bash
$ systemd-analyze plot > plot.svg
vagrant@ubuntu-bionic:~$ systemd-analyze critical-chain
The time after the unit is active or started is printed after the "@" character.
The time the unit takes to start is printed after the "+" character.

graphical.target @19.281s
└─multi-user.target @19.277s
  └─snapd.seeded.service @17.562s +1.649s
    └─snapd.service @13.940s +3.619s
      └─basic.target @13.573s
        └─sockets.target @13.572s
          └─snapd.socket @13.299s +264ms
            └─sysinit.target @13.233s
              └─cloud-init.service @10.160s +3.052s
                └─systemd-networkd-wait-online.service @8.865s +1.285s
                  └─systemd-journald.socket @1.984s
                    └─system.slice @1.980s
                      └─-.slice @1.923s
```

### Organisation plus souple

`systemd` propose une organisation plus aboutie des services à démarrer aux boot.  
Là ou systemV proposait au maximum 4 niveaux de démarrage (les niveaux 2 3 4 5), chacun modélisé par une liste de services à démarrer, `systemd` propose une organisation arborescente de *targets* et d'*unités*. Chaque *target* intègre des *unités* à démarrer ou d'autres *targets* (pouvant à leur tour contenir des *unités*).

**Une *unité* correspond à une ou plusieurs ressources du système** : un *service*, une configuration comme un système de fichiers ou une config IP d'une carte réseau, l'activation d'un socket TCP, etc.

Une *target* est une *unité* spéciale regroupant les autres *unités* dans un niveau de service du système. L'ensemble des *unités* nécessairement activées pour atteindre ce niveau de service.

En général, la *target* par défaut est `multi-user.target` ou `graphical.target` (système avec GUI).

### Complétude

`systemd` offre de grandes possibilités de gestion du système en raison de :

- son intégration avec [D-Bus](./definitions.md#dbus) et la publication d'API `systemd` qui y est faite
- sa gestion intégrée des [cgroups](./definitions.md#cgroups) et des [namespaces](./definitions.md#namespaces)

### Mais encore

`systemd` intègre aussi de façon modulaire d'autre composants des systèmes UNIX en les remplaçant par des solutions plus modernes et normalisées. 

Liste non-exhaustive :

#### Daemons

- `crond` : avec des *unités* de type `.timer`
- `xinetd` : avec des *unités* de type `.socket`
- la syslog (`rsyslogd`) : avec le daemon `journald`

#### Fichiers

- inittab : avec les *unités* de type `.target`
- le fichier `/etc/fstab` : avec des *unités* de type `.mount`

### Outils de gestion système propres à chaque distribution

- script de gestion réseau et/ou le daemon `NetworkManager` 
- daemon `systemd-networkd` 
- les outil `systemd-resolved`, `hostnamectl`

## Utilisation

### `systemctl`

`systemctl` pour simplement activer (`enable`), démarrer (`start`), désactiver (`disable`) ou arrêter (`stop`) un *service* :

```bash
root@ubuntu-bionic:~# systemctl enable apache2@.service
Created symlink /etc/systemd/system/multi-user.target.wants/apache2@.service → /lib/systemd/system/apache2@.service.
root@ubuntu-bionic:~# systemctl start apache2
root@ubuntu-bionic:~# systemctl status apache2
● apache2.service - The Apache HTTP Server
   Loaded: loaded (/lib/systemd/system/apache2.service; enabled; vendor preset: enabled)
  Drop-In: /lib/systemd/system/apache2.service.d
           └─apache2-systemd.conf
   Active: active (running) since Mon 2237-06-19 21:22:41 UTC; 2min 27s ago
 Main PID: 17107 (apache2)
    Tasks: 55 (limit: 1152)
   CGroup: /system.slice/apache2.service
           ├─17107 /usr/sbin/apache2 -k start
           ├─17108 /usr/sbin/apache2 -k start
           └─17109 /usr/sbin/apache2 -k start

Jun 19 21:22:41 ubuntu-bionic systemd[1]: Starting The Apache HTTP Server...
Jun 19 21:22:41 ubuntu-bionic apachectl[17086]: AH00558: apache2: Could not reliably determine the
Jun 19 21:22:41 ubuntu-bionic systemd[1]: Started The Apache HTTP Server.
```

Déterminer la *target* par défaut :

```bash
# systemctl get-default
multi-user.target
```

Définir une *target* par défaut :

```bash
# systemctl set-default multi-user.target  
```

## Configuration `systemd`

### Les composants

La configuration est dans : `/etc/systemd/`.

On y retrouve, la configuration des composants `system.conf`, `journald.conf`, `resolved.conf`, etc. Ces fichiers contiennent à chaque fois les valeurs par défaut commentées que nous pouvons alors dé-commenter et éditer.

```bash
vagrant@ubuntu-bionic:~$ find /etc/systemd/ -name "*.conf"
/etc/systemd/timesyncd.conf
/etc/systemd/journald.conf
/etc/systemd/resolved.conf
/etc/systemd/system.conf
/etc/systemd/user.conf
/etc/systemd/logind.conf
```

En consultant le `man` de `journald.conf` par exemple :

```bash
NAME
       journald.conf, journald.conf.d - Journal service configuration files

SYNOPSIS
       /etc/systemd/journald.conf
       /etc/systemd/journald.conf.d/*.conf
       /run/systemd/journald.conf.d/*.conf
       /usr/lib/systemd/journald.conf.d/*.conf
DESCRIPTION
       These files configure various parameters of the systemd journal service, systemd-
       journald.service(8).
```

On se rend compte que cette config peut être surchargée à plusieurs endroits.

> Ces composants sont déja correctement configurés, il ne me semble pas nécessaire d'étudier cette configuration maintenant.

### Les *unités* et *targets*

La sous-arborescence `/etc/systemd/system/` contient la définition effective des *targets* et *unités*. C'est la configuration courante proposée par l'éditeur de la distribution GNU/Linux et maintenue par l'adminsys.  
Les *unités* et *targets* peuvent être redifinies ici, surchargées ou complémentées (*via* la création d'un dossier de même nom mais suffixé `.d`). Ils peuvent aussi n'être que des liens symboliques vers les définition connue (par `systemd`) des ces *unités* dans l'arborescence `/lib/systemd` ou `/usr/lib/systemd/system`.

Cette sous-arborescence définie les interdépendances entres les *unités*. Certaines dépendances comme pour les *unités* de type ***target*** seront représentées par des dossiers `xxxx.wants`, contenant les *unités* devant être démarrées avec l'*unité* *target*. D'autres pourront disposer d'un dossier `xxxx.requires` contenant d'autres *unités* requises par la présente *unité*.

```bash
root@ubuntu-bionic:~# ls -al /etc/systemd/system/
total 64
drwxr-xr-x 15 root root 4096 Jun 19 21:21 .
drwxr-xr-x  5 root root 4096 Apr  3  2020 ..
drwxr-xr-x  2 root root 4096 Apr  3  2020 cloud-final.service.wants
drwxr-xr-x  2 root root 4096 Apr  3  2020 cloud-init.target.wants
-rw-r--r--  1 root root  251 Nov 16  2017 cron.service
lrwxrwxrwx  1 root root   44 Apr  3  2020 dbus-org.freedesktop.resolve1.service -> /lib/systemd/system/systemd-resolved.service
drwxr-xr-x  2 root root 4096 Apr  3  2020 default.target.wants
drwxr-xr-x  2 root root 4096 Apr  3  2020 final.target.wants
drwxr-xr-x  2 root root 4096 Apr  3  2020 getty.target.wants
drwxr-xr-x  2 root root 4096 Apr  3  2020 graphical.target.wants
lrwxrwxrwx  1 root root   38 Apr  3  2020 iscsi.service -> /lib/systemd/system/open-iscsi.service
drwxr-xr-x  2 root root 4096 Jun 19 21:24 multi-user.target.wants
drwxr-xr-x  2 root root 4096 Apr  3  2020 network-online.target.wants
drwxr-xr-x  2 root root 4096 Apr  3  2020 open-vm-tools.service.requires
drwxr-xr-x  2 root root 4096 Apr  3  2020 paths.target.wants
drwxr-xr-x  2 root root 4096 Apr  3  2020 sockets.target.wants
lrwxrwxrwx  1 root root   31 Jun 19 21:21 sshd.service -> /lib/systemd/system/ssh.service
drwxr-xr-x  2 root root 4096 Apr  3  2020 sysinit.target.wants
lrwxrwxrwx  1 root root   35 Apr  3  2020 syslog.service -> /lib/systemd/system/rsyslog.service
drwxr-xr-x  2 root root 4096 Apr  3  2020 timers.target.wants
```

Nous retrouvons donc :

- Des *unités* : des fichiers ou des liens symboliques vers les définition de ces *unités*
- Des dossiers `$target$.target.wants/` contenant les *unités* (fichiers ou liens) qui constituent alors les *unités* de type *target*.
- Des dossiers `$unit$.$unittype$.requires/` contenant des liens vers les *services* dont dépendent *l'unité*

Les *unités* connues par le système peuvent être :

- **`enable`** : existant dans `/etc/systemd/system`, en tant que fichier ou lien symbolique vers sa définition
- **`disable`** : inexistant dans `/etc/systemd/system`, mais avec une définition dans `/lib/systemd/system` ou `/usr/lib/systemd/system`
- **`masked`** : existant en tant que lien depuis `/etc/systemd/system` mais pointant vers `/dev/null` plutot que sa définition (interdit à l'activation)

### Configuration génériques des unités

Les *unités* `systemd` sont donc des définitions de ressources système.

L'objectif de `systemd` est de maintenir leur état souhaité :

- *Active* : pour démarré (donc l'*unité* est *enabled* ou *loaded*)
- *Inactive* : si arrêté  (normal pour une *unité* *disabled* ou après un arrêt manuel)
- *Failed* : si le démarrage de *l'unité* échoue

Il en existe plusieurs types :

- `$unit$.target`,
- `$unit$.service`,
- `$unit$.socket`,
- `$unit$.device`,
- `$unit$.mount`,
- `$unit$.timer`,
- `$unit$.path`,
- ...

Elles sont définies au travers de fichiers type `.ini` :

```ini
[section]
token = value
othertoken = anothervalue
```

On retrouve leur documentation dans les manuels associés.

Pour les *unités* dans leur globalité :

```bash
man systemd.unit
```

Ou pour les *unités* de type $unittype$ :

```bash
man systemd.unittype

# Par exemple
man systemd.service
```

#### Les sections

En regardant dans l'ensemble des *unités* on peut retrouver les sections existantes :

```bash
root@ubuntu-bionic:~# grep -r "\[" /etc/systemd/system/ /usr/lib/systemd/ /lib/systemd/system/ | grep -v "^ +#" | cut -d[ -f2 | cut -d\] -f1 | sort | uniq -c | sort -g | tail -7
      5 Path
      6 Mount
      9 Timer
     27 Socket
     87 Install
    150 Service
    266 Unit
```

Les principales sections des *unités* sont :

- `[Unit]` : la définition de *l'unité*
- `[Service]` : la définition de la gestion d'un *service* (démarrage, arrêt, reload...)
- `[Install]` : la définition de l'activation d'une *unité* (où elle se situe dans l'arborescence des unités configurées (`/etc/systemd/system`)

> `[Path]`, `[Mount]`, `[Timer]`, `[Socket]` sont des sections dédidées aux unités de ces type.

#### Les tokens de configuration des unités

Il est possible de compter les tokens existants dans les différentes *unités* et de récupérer les plus utilisés. Cela nous permet d'identifier les principaux :

```bash
root@ubuntu-bionic:~# grep -r "=" /etc/systemd/system/ /usr/lib/systemd/ /lib/systemd/system/ | grep -v "^ +#" | cut -d: -f2 | cut -d= -f1 | sort | uniq -c | sort -g | tail -30
     12 EnvironmentFile
     12 IgnoreSIGPIPE
     12 SystemCallArchitectures
     13 ConditionDirectoryNotEmpty
     13 StandardOutput
     15 StopWhenUnneeded
     16 AllowIsolate
     16 RefuseManualStart
     17 SocketMode
     19 ListenStream
     21 TimeoutSec
     22 Restart
     23 ConditionVirtualization
     23 Environment
     23 ExecStop
     24 KillMode
     28 ConditionKernelCommandLine
     39 Wants
     46 RemainAfterExit
     52 ConditionPathExists
     55 Requires
     67 Conflicts
     78 WantedBy
    114 Before
    124 Type
    140 DefaultDependencies
    153 ExecStart
    156 After
    217 Documentation
    265 Description
```

Pour la section **`[Unit]`** : la description de *l'unité* et la définition de ses dépendances et interdependances avec les autres *unités* : tous les tokens non liés au type spécifique *d'unité*

- `Description`,
- `Documentation`,
- `Requires` : les *unités* qu'il faut absolument démarrer pour démarrer celle-ci,
- `Requisite` : les *unités* qui doivent déjà être démarrées afin de pouvoir démarrer celle-ci
- `wants` : les *unité s*que l'on essaye de démarrer avec celle-ci
- `Conflict` : la liste des *unités* en conflit avec celle-ci et qui seront donc stoppées
- `Before`, `After` : les *unités* devant être démarrées avant ou après celle-ci
- `DefaultDependencies`: `yes` par défaut et `no` sinon. Lorsque mis à `no`, ce token altère la gestion des dépendances. Exemple : une *unité* de type *target* démarre toutes les *unités* requises (`Requires`) ou souhaitées (`Wants`) sauf si elles sont elles-mêmes spécifiées avec le `defaultDepenencies` à `no`.

Pour la section **`[Install]`** : on définit ici comment cette *unité* sera activée, où elle sera placée dans l'arborescence des *targets* dans `/etc/systemd/system`

- `WantedBy` : dans quel dossier `$target$.target/wants` cette *unité* doit être définie
- `RequiredBy` : dans quel dossier `$unit$.$unittype$.requires` cette *unité* doit être définie
- `Also` : quelles *unités* doivent être installées ou désinstallées en même temps que celle-ci
- `Alias` : un second nom pour ce service sera défini dans `/etc/systemd/system` (un autre lien symbolique), le *service* sera alors activé sous ses deux noms

Les autres sections sont attachées aux types *d'unité* et seront abordées plus bas, dans les sections dédiées à chaque type *d'unité*.

#### Commande pratiques de gestion des spécifications *d'unités*

Voir une *unité* `systemd` :

```bash
$ systemctl cat sshd.service
.../...
$
```

Ou l'éditer :

```bash
$ sudo systemctl edit --full cron.service
.../...
$
```

Cette commande crée une copie de la définition de *l'unité* (depuis `/lib` ou `/usr/lib`) vers le dossier `/etc/systemd/system` afin de permettre une modification de ses spécifications.

On peut aussi créer une surcharge sur *l'unité* existante :

```bash
$ sudo systemctl edit docker.service
.../...
$
```

Cette commande créera un dossier drop-in `/etc/systemd/system/$name$.service.d/override.conf` permettant d'ajouter du contenu ou de surcharger les *unités* par défaut.

### Les *unités* de type *target*

Sous `systemd`, une `target` remplace la notion de run level, il est défini :

- par une *unité* spéciale `$target$.target` (sous `/lib/systemd/system`) définissant la `target` et ses dépendances
- et si activé, un dossier `/etc/systemd/system/$target$.target.wants` contenant des liens vers les fichiers de définition des *unités* `systemd` constituant cette *target*

Exemple *d'unité* de type *target* :

```bash
root@ubuntu-bionic:~# cat /lib/systemd/system/multi-user.target
#  SPDX-License-Identifier: LGPL-2.1+
#
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

[Unit]
Description=Multi-User System
Documentation=man:systemd.special(7)
Requires=basic.target
Conflicts=rescue.service rescue.target
After=basic.target rescue.service rescue.target
AllowIsolate=yes
```

Son dossier sous `/etc/systemd/system/` une fois activé sur le système :

```bash
# ls -al /etc/systemd/system/multi-user.target.wants
total 8
drwxr-xr-x.  2 root root 4096 28 avril 16:29 .
drwxr-xr-x. 11 root root 4096 28 avril 16:33 ..
lrwxrwxrwx.  1 root root   38 28 avril 16:29 auditd.service -> /usr/lib/systemd/system/auditd.service
lrwxrwxrwx.  1 root root   37 28 avril 16:28 crond.service -> /usr/lib/systemd/system/crond.service
lrwxrwxrwx.  1 root root   41 28 avril 16:29 firewalld.service -> /usr/lib/systemd/system/firewalld.service
lrwxrwxrwx.  1 root root   42 28 avril 16:29 irqbalance.service -> /usr/lib/systemd/system/irqbalance.service
lrwxrwxrwx.  1 root root   37 28 avril 16:29 kdump.service -> /usr/lib/systemd/system/kdump.service
lrwxrwxrwx.  1 root root   46 28 avril 16:28 NetworkManager.service -> /usr/lib/systemd/system/NetworkManager.service
lrwxrwxrwx.  1 root root   39 28 avril 16:29 postfix.service -> /usr/lib/systemd/system/postfix.service
lrwxrwxrwx.  1 root root   40 28 avril 16:28 remote-fs.target -> /usr/lib/systemd/system/remote-fs.target
lrwxrwxrwx.  1 root root   46 28 avril 16:28 rhel-configure.service -> /usr/lib/systemd/system/rhel-configure.service
lrwxrwxrwx.  1 root root   39 28 avril 16:29 rsyslog.service -> /usr/lib/systemd/system/rsyslog.service
lrwxrwxrwx.  1 root root   36 28 avril 16:29 sshd.service -> /usr/lib/systemd/system/sshd.service
lrwxrwxrwx.  1 root root   37 28 avril 16:29 tuned.service -> /usr/lib/systemd/system/tuned.service
```

Ici la *target* `multi-user.target` activée sur un CentOS7, représentée par un dossier `.wants` dans `/etc/systemd/system`.

### *Unité* de type *service*

Les scripts de démarrage historiques (non encore intégrés à `systemd`) exécutaient normalement toujours les mêmes actions :

- Démarrer le processus avec certains arguments en gérant
  - Ses entrée/sorties
  - Son PID (sauvegardé dans un fichier)
  - Ses éventuelles dépendances
- Envoyer un signal avec `kill` sur le PID (pour reload ou pour stopper le service)

`systemd` a intégré ces actions et permet lors de l'intégration d'un produit dans une distribution de ne plus avoir à re-créer un script de démarrage.

Exemple *d'unité* :

```bash
vagrant@ubuntu-bionic:~$ systemctl cat sshd.service
# /lib/systemd/system/ssh.service
[Unit]
Description=OpenBSD Secure Shell server
After=network.target auditd.service
ConditionPathExists=!/etc/ssh/sshd_not_to_be_run

[Service]
EnvironmentFile=-/etc/default/ssh
ExecStartPre=/usr/sbin/sshd -t
ExecStart=/usr/sbin/sshd -D $SSHD_OPTS
ExecReload=/usr/sbin/sshd -t
ExecReload=/bin/kill -HUP $MAINPID
KillMode=process
Restart=on-failure
RestartPreventExitStatus=255
Type=notify
RuntimeDirectory=sshd
RuntimeDirectoryMode=0755

[Install]
WantedBy=multi-user.target
Alias=sshd.service
```

Dans la section **`[Service]`** de *l'unité* : on spécifie simplement le *service*.

Le type de *service* est défini *via* le token **`Type`** décrivant comment le processus va se comporter une fois allumé (utile pour le monitoring du service notamment) :

- `**simple**` : un daemon (`sshd`)
- `forking` : une grappe de processus (`httpd`)
- `dbus` : un daemon avec un nom D-Bus
- `notify` : un daemon qui notifie la fin de son démarrage à `systemd`
- `idle` : un daemon qui sera démarré à la fin du processus de démarrage complet de la *target* (au pire dans 5 secondes)
- `**oneshot**` : une simple commande a lancer une seule fois, le token `RemainAfterExit = yes` précise alors que l'état du service est maintenu une fois la commande passée :
  - avec un code retour 0 : `active`
  - avec un autre code retour : `failed`

La spécification des commandes permettant le démarrage, l'arrêt ou le reload de la config :

- `ExecStop`
- `ExecStart`
- `ExecReload`
- `ExecStartPre`, `ExecStartPost`, `ExecStopPost`, etc.

Les spécifcations nécessaires à l'exécution de celle-ci :

- `TimeoutSec` : le timeout
- `Restart` : est-ce que `systemd` redémarre le service s'il plante : `no`, `on-success`, `on-failure`, `on-abnormal`, `on-watchdog`, `on-abort`, ou `always`
- `KillMode` : comment le service est killé si le stop échoue
- `StandardOutput`, `StandardError` : comment on gère les sorties du processus (voir `man systemd.exec`)
- `Environment` : permet de valoriser des variables d'environnement (voir `man systemd.exec`)
- `EnvironmentFile` : idem mais les variables sont spécifiées dans un fichier
- etc.

### *Unité* conditionnant l'activation d'un *service*

#### *Unité* de type *socket*

Le *socket-based activation* vient remplacer la daemon `xinetd`.  
Pour rappel, `xinetd` est le super serveur sous GNU/Linux. Il lit un ensemble de spécifications de services réseau, et ouvre les sockets réseau correspondants auprès du kernel, en leur nom. Si une requête réseau est reçue sur le socket alors le processus correspondant est lancé et la requête lui est transmise.

Le daemon `systemd` propose le même type de solution.

On garde la spécification de *service* mais le service n'est pas activé *(enabled)*.

`/etc/systemd/system/helloworld.service` :

```ini
[Unit]
Description=socket based helloworld Service
After=network.target helloworld.socket
Requires=helloworld.socket

[Service]
Type=simple
Environemennt=FLASK_APP=app.py
WorkingDirectory=/opt/app
ExecStart=flask run --host=0.0.0.0
TimeoutStopSec=5

[Install]
WantedBy=default.target
```

L'unité de type *socket* déclanchant le *service* :

`/etc/systemd/system/helloworld.socket` :

```ini
[Unit]
Description=helloworld Socket
PartOf=helloworld.service

[Socket]
ListenStream=127.0.0.1:5000

[Install]
WantedBy=sockets.target
```

```bash
# systemctl disable --now helloworld.service
# systemctl enable --now helloworld.socket
#  
```

Pour avoir plus d'infos sur les tokens de la section `[Socket]`

```bash
$ man systemd.socket
.../...
$
```

#### Unité de type *timer*

Les *unités* de type *timer* offrent une alternative à cron.

Les tokens de spécification *timer* utilisés :

```bash
root@ubuntu-bionic:~# grep -o "^[a-z|A-Z]*=" /etc/systemd/system/*.wants/*.timer | cut -d: -f2 | cut -d= -f1 | sort | uniq -c | sort -n
      1 After
      1 ConditionKernelCommandLine
      1 ConditionVirtualization
      1 Documentation
      2 AccuracySec
      2 OnStartupSec
      4 RandomizedDelaySec
      5 Description
      5 OnCalendar
      5 Persistent
      5 WantedBy
```

On étudiera les possibilités dans le `man` :

```bash
$ man systemd.timer
.../...
$
```

Voici un exemple d'utilisation :

- On garde une définition de *service*

```ini
# /etc/systemd/system/backup.service
[Unit]
Description=script de backup

[Service]
Type=oneshot
ExecStart=/opt/backup/backup.sh

[Install]
WantedBy=multi-user.target
```

- Puis une activation *via* une *unité* de type *timer* :

```ini
# /etc/systemd/system/backup.timer
[Unit]
Description=Start backup.sh daily

[Timer]
OnCalendar=*-*-* 04:00:00

[Install]
WantedBy=timers.target
```

- On dispose aussi d'un vue globale sur les *timers* déclenchés par `systemd` :

```bash
root@ubuntu-bionic:~# systemctl list-timers
NEXT                         LEFT          LAST                         PASSED       UNIT
Thu 2020-04-09 22:56:09 UTC  5h 43min left n/a                          n/a          motd-news.tim
Thu 2020-04-09 23:47:49 UTC  6h left       n/a                          n/a          apt-daily.tim
Fri 2020-04-10 06:03:40 UTC  12h left      n/a                          n/a          apt-daily-upg
Fri 2020-04-10 15:40:53 UTC  22h left      Thu 2020-04-09 15:40:53 UTC  1h 31min ago systemd-tmpfi
Mon 2020-04-13 00:00:00 UTC  3 days left   n/a                          n/a          fstrim.timer
```

#### *Unité* de type *path*

Cette *unité* permet de déclencher un *service* sur une modification de fichier !

*Unité* `/etc/systemd/system/replicate.path` :

```ini
[Unit]
Description=check for filechange

[Path]
PathChanged=/opt/backup/database.dump
Unit=replicate.service

[Install]
WantedBy=multi-user.target
```

Le *service* qui sera déclenché `/etc/systemd/system/replicate.service` :

```ini
[Unit]
Description=replicate when file has changed.

[Service]
Type=oneshot
ExecStart=/opt/backup/replicate.sh

[Install]
WantedBy=multi-user.target
```

Le script `/opt/backup/replicate.sh` :

```bash
#!/bin/bash
while pidof /opt/backup/backup.sh
do
  sleep 10
done
scp /opt/backup/database.dump backup@othersite.away.com:/opt/replicated/database.dump
```

# Docker

La complexification des environnements applicatifs nous oblige à disposer de plusieurs environnements de developement ou de test lorsque l'on souhaite travailler sur plusieurs projets et donc de respecter les contraintes de chacun d'entre eux.

Ces environnements utilisent des composants différent voir identiques mais de versions différentes. Des solutions permettent de faire coexister plusieurs environnements sur le même host, voir pour le même compte, mais leur gestion est leur adminsitration est complexe et un peu pénible.

Docker apporte une solution souple, facile et commune à tous les projets pour gèrer ces envrionnements multiples.

## Présentation de docker

Un système de conteneurisation docker est constitué d'un noyau linux disposant de fonctionalité de conteneurisation, d'un daemon docker et d'un ensemble de commandes permettant d'orchestrer tout ceci.

![container-function](./images/docker-run.png)

le terminal permet de passer les commandes docker qui utilisent l'api porté par dockerd, dockerd envois les ordre au kernel qui démarre le processus conteneurisé et connecte, si besoin, celui-ci au terminal.

## Environnement de travail

Pour passer les commandes qui suivent, si vous ne disposez pas déja d'un environnement docker, je vous propose d'installer une simple VM sous centOS avec comme d'habitude, 2 interfaces réseaux nat + host_only. Vous vous connecterez en ssh à la VM, et vous installerez docker en suivant la [documentation officiel docker](https://docs.docker.com/).

vous pouvez aussi utilisé le Vagrantfile suivant (il est un pe long soyez patient les warning ne sont pas problématique) :

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vm.network "private_network", ip: "192.168.33.5"
  config.vm.provider "virtualbox" do |vb|
     vb.memory = "2048"
  end
  config.vm.provision "shell", inline: <<-SHELL
    yum update -y -q
    yum install -y -q yum-utils device-mapper-persistent-data lvm2 bridge-utils
    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    yum install -y -q docker-ce docker-ce-cli containerd.io
    firewall-cmd --add-masquerade --zone=public --permanent
    systemctl enable --now firewalld
    systemctl enable --now docker
    usermod -aG docker vagrant
  SHELL
  config.vm.provision compose type: "shell", inline: <<-SHELL
    curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
  SHELL
end
```

### Installation sur votre OS

Vous pouvez si vous prèfèrez utiliser l'installation sur votre laptop, celle-ci est différente suivant votre système d'exploitation, n'allez pas chercher de vieux tuto (hein Florian?), consulter la [documentation officiel docker](https://docs.docker.com/)

### Validation

il suffit d'instancier le container de test hello-world :

```bash
[root@localhost ~]# docker run hello-world
```

Sortie de la commande docker :

```bash
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
1b930d010525: Pull complete
Digest: sha256:f9dfddf63636d84ef479d645ab5885156ae030f611a56f3a7ac7f2fdd86d7e4e
Status: Downloaded newer image for hello-world:latest
```

L'image hello world n'existe pas en locale et est donc téléchargée.

Sortie du container hello-world :

```bash
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
[root@localhost ~]#
```

Docker fonctionne :

1 - le client (la commande) a contacté le daemon,  
2 - l'image hello-world à été téléchargée (pull),  
3 - un container à été **créer** et exécuté et enfin  
4 - le programme exécuté dans le container a transmi sa sortie au terminal

## Utilisation des containers

> vous exécuterez chacune des commandes suivante sur votre environnement.

### manipulation des containers

- Lister les container présent : `docker ps` (option -a pour voir les container arrêté)
- Démarrage d'un container existant : `docker start`
- Instanciation d'un container : `docker run`
- consultation des logs d'un container : `docker logs`
- Modification d'un container pendant son exécution : `docker exec`
- Supression d'un container : `docker rm`

__docker ps:__ Permet de lister les containers

```bash
[vagrant@localhost ~]$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
90664724abc6        hello-world         "/hello"                 2 minutes ago        Exited (0) 2 minutes ago                         adoring_moser
```

Un seul container à été lancé et est maintenant stoppé (le container hello-world ne fait qu'afficher le message hello from docker et s'arrête), on retrouve :

- son id : 90664724abc6
- l'image utilisée : hello-world
- la commande passée : /hello
- sa date de création (instanciation)
- son statut : terminé avec son code retour (et depuis combien de temps)
- son nom généré : adoring_moser

__docker start:__ permet de relancer un container

En **récupérant l'id du container** dans la sortie de la commande `docker ps -a` on peu le relancer :

```bash
[vagrant@localhost ~]$ docker start -a 90664724abc6

Hello from Docker!
This message shows that your installation appears to be working correctly.
.../...
```

Ici nous relançons un container déja existant. l'option -a (attach) permet de récupèrer sa sortie standard sur le terminal courant.

__docker run:__ permet d'instancier un container

Ci dessous on instancie un nouveau container en mode daemon ou détaché (option -d). On utilise une image alpine dans laquelle on lance un mini script shell qui tourne pendant 30 secondes pour former le container:

```bash
[vagrant@localhost ~]$ docker run -d alpine sh -c 'echo starting ; sleep 30 ; echo stop'
ff4f7aad8c49f338534db7f9b6e3d4b8a8968ecdedfef71276dcaccccb4ae954
```

docker nous retourne alors son id, il s'exécute en arrière plan.

`docker ps` le vois donc en cours d'exécution (pendant les 30 secondes) :

```bash
[vagrant@localhost ~]$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
ff4f7aad8c49        alpine              "sh -c 'echo startin…"   3 seconds ago       Up 2 seconds                            vigilant_franklin
```

son statut est bien **Up**

> alpine est une distribution très légère orienté sécurité et trés utilisée pour construire des containers [site officiel](https://www.alpinelinux.org/)

__docker logs:__ permet de consulter les logs du container

On pourra aussi consulter ses sorties standard et d'erreur avec la commande docker logs (sur son id récupèrer dans la sortie de docker ps)

```bash
[vagrant@localhost ~]$ docker logs ff4f7aad8c49
starting
stop
```

On peu toujours le relancer (on utilise cette fois plutot sont nom généré plutot que son id, les deux fonctionnent) :

```bash
[vagrant@localhost ~]$ docker start vigilant_franklin
ff4f7aad8c49
```

Si on reconsulte les logs, on peu constater que plusieurs exécutiuon on bien été effectué:

```bash
[vagrant@localhost ~]$ docker logs vigilant_franklin
starting
stop
starting
stop
```

__docker exec:__ Permet d'attacher un processus supplémentaire au container.

On instancie un nouveau container proche du précédent mais avec s'exécutant pendant 999 secondes cette fois ci :

```bash
[vagrant@localhost ~]$ docker run -d alpine sh -c 'echo starting ; sleep 999 ; echo stop'
0289f2c461a5060196a9070d0223084f64cbc5baac62cd974c54782b57afc5db
```

On **ajoute** un processus supplémentaire au container avec docker exec et on y attache notre terminal (option -it pour **i**nteractive et avec un **t**ty) Cela est necessaire si on souhaite y accèder. On attache un shell au container.

```bash
[vagrant@localhost ~]$ docker exec -it 0289f2c461a5060196 sh
/ # ps
PID   USER     TIME  COMMAND
    1 root      0:00 sh -c echo starting ; sleep 999 ; echo sto
    6 root      0:00 sleep 999
    7 root      0:00 sh
   12 root      0:00 ps
/ # ls /
bin    etc    lib    mnt    proc   run    srv    tmp    var
dev    home   media  opt    root   sbin   sys    usr
```

__docker rm:__

On consulte les instances de containers avec docker ps :

```bash
[vagrant@localhost ~]$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                        PORTS               NAMES
0289f2c461a5        alpine              "sh -c 'echo startin…"   9 minutes ago       Up 9 minutes                                      elegant_satoshi
ff4f7aad8c49        alpine              "sh -c 'echo startin…"   12 minutes ago      Exited (0) 12 minutes ago                         vigilant_franklin
90664724abc6        hello-world         "/hello"                 15 minutes ago      Exited (0) 15 minutes ago                         adoring_moser
```

On supprime alors les instances arrêté :

```bash
[vagrant@localhost ~]$ docker ps -a | awk '$1~/^[0-9|a-z]*$/ {print "docker rm "$1}' | bash
```

### Pour conclure sur la manipulation des containers

Nous savons donc maintenant :

- instancier des container (docker run)
- redémarrer un container (docker start)
- s'y connecter en attachant un processus supplémentaire (docker exec)
- consulter sa sortie (docker logs)
- et enfin les supprimer

> Vous aller re-instancier une image alpine avec un processus sleep 999 en mode daemon.
  Dans un second terminal vous identifier le processus sleep vue du docker host.
  Puis vous lancer un shell sh attaché au container et vous regarder le processus vue du container avec la commande ps.

# Troubleshooting

C'est l'activité principale des équipe opérationnelles : anticiper, eviter et résoudre les incidents et problemes

Rappels : résoudre un incident consiste à remettre le service en fonction, traiter le probleme consiste à chercher la cause de l'incident et de proposer des solution à long terme.

## Anomalie courante et connues

- Service arreté
- Saturation de ressources : réseaux disque cpu ram
- Regression suite à un changement (vraiment fréquent)
- La couche 8 du modèle osi (encore plus fréquent!)

## Principe de traitement d'une anomalie inconnue

### Qualification

Il s'agit de definir précisément quel est l'anomalie rencontré.

- __vérifier qu'il s'agit bien d'une anomalie__ : le service à-t-il déja été fonctionnel? cette fonctionnalité est-elle prévue ?
- Lorsque cela est possible de trouver une méthode ou une __procédure de reproduction de l'anomalie__.
- Sinon obtenir un horodatage précis d'une ou plusieurs occurences de l'anomalie

Attention nous rencontrons vraiment des perles à ce niveau là vérifier même l'évident!

### Analyse

Collecte d'informations **et des traces** en suivant le protocole de reproduction de l'anomalie ou via l'horodatage de l'anomalie déclarée.

En reprenant les principes de fonctionnement du service impacté et validant point par point les éléments rencontrés.

Pour le réseaux : les couches du modèle OSI :

- Niveau physique (vérifiez les connexions surtout si elle sont virtuelles et accessibles)
- Verifiez la liaison (table arp)
- Le réseaux (icmp ping si supporté)
- Le Transport (telnet sur le port tcp ou tcpdump sur la cible pour voir les packets arriver)
- Et enfin le tester le protocole applicatif pour sessions, presentation et application.

Pour un service réseaux, coté client :

- Validez la résolution dns du service (dig, host, nslookup), et récupèrer l'ip.
- Validez le réseaux entre le client et cette ip.

Coté server, après avoir écarter les problématiques de transport réseaux du client jusque l'infratructure, il conviendra de suivre élément par élément l'accès du client vers le serveur en tenant compte élément éventuel suivant :

- du ou des nat entrant (log routeur et firewall)
- du reverse proxy (log du reverse proxy)
- du serveur applicatif (log du service)
- de l'application
- et enfin de l'accès à ses données

Bien sur dans le cas d'une annomalie purement applicative, il ne sera pas nécessaire de vérifier que l'accès à l'application est fonctionnel, encore que

### Correction

Pour la résolution de l'incident, une opération simple peu être envisagé sur la production (redémarrage d'un service par exemple), proposer une solution de contournement de l'anomalie.

Cependant une modification de l'infra devra repecter les principes de la gestion des changements en production, dans l'objectif de maitriser l'impacte de la correction sur la production.

Après avoir valider la non regression dans un environment hors production, vous vous preparrer à exécuter l'opération en production.

Celle-ci sera planifier en concertation avec la maitrise d'ouvrage.

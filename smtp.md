# Simple Mail Transfert Protocol

## Presentation

la messagerie mail est le premier service internet.
Le mail à été inventé dans les annèes 60 permettant de se laisser des messages d'un compte à l'autre sur le même hosts.
Dans les annèes 70 le premier mail inter hosts sur l'arpanet est emis 2 années plus tard les mails représente 75% du trafique réseaux.

### Les protocoles

#### SMTP

Le protocole d'envoi et d'acheminement des mails il est utilisé pour :

- Les envois de mail depuis un client de messagerie
- le transfert de mail inter-serveurs de messagerie

Il utilise pour  un accès non authentifié, car c'est un service ouvert (d'ou les SPAM)

Protocole s'apuyant sur TCP/IP sur le port 25 ; normé par la RFC 6409 et ancienement RFC 2476 enfin ESMTP (extended SMTP) est définie par la RFC 3207

Le protocole est dialogue client serveur : commande, code retour et les opérations sont effectuées via une série de commandes.

voici les codes retour principaux :

- 220 connexion OK
- 250 commande acceptée.
- 354 Le serveur attend des données. Le client termine par un point seul sur une ligne
- 421 Échec temporaire au niveau de la connexion. (retry later)
- 550 Échec permanent. l'adresse du destinataire est invalide.
- 554 Échec permanent connexion refusé (blacklist ?)

#### LMTP

LMTP (Local Mail Transfer Protocol) : Le protocole de livraison de messagerie locale (dans le boite mail)

c'est un protocole local au travers d'un socket.

#### Les protocoles d'acces (Mail Access Protocol)

__POP :__ Post Office Protocol, l'historique, il permet de récupérer les nouveaux messages de sa boite mail vers son client local.

__IMAP :__ Internet Message Access Protocol, il permet de gèrer une boite mail distante(lecture des entête et message, suppression, déplacement gestion de répertoires) de puis un client.

Ces protocoles sont authentifiés mais non chiffrés il peuvent être encapsuler dans du TLS : IMAPs POPs

### Le format des mails

__Le header :__ liste d'entête suivis de données founissant des Informations sur le transport et le format du mail
Il est possible de définir et d'ajouter des en-têtes supplémentaires personnalisés : X-Entete

```bash
From: "Me" <from@fromdomain.net>
Return-Path:<from@fromdomain.net>
Envelope-to: dest@destdomain.com
Delivery-date: Sat, 6 Oct 2017 10:53:12 +0200
Received: from mta.destdomain.net (mta.destdomain.net [84.16.72.98])
    by mda.destdomain.net (8.13.8/8.13.8) with ESMTP id l968qB9A032394
    for <dest@destdomain.net>; Sat, 6 Oct 2017 10:52:11 +0200
Received: from mta.fromdomain.net (mta.fromdomain.net [195.186.19.82])
    by mta.destdomain.net (8.13.8/8.13.8) with ESMTP id l968qBMM021452
    for <dest@destdomain.net>; Sat, 6 Oct 2017 10:52:11 +0200
Received: from MUA-name (80.218.xxx.x) by mta.fromdom.net (MUA-soft version)
id 46F907080029C248; Sat, 6 Oct 2017 08:50:35 +0000
Message-ID: <3FC7D4809F4C4CA68EC4C83FB483A0D1@MUA-hostname>
Date: Sat, 6 Oct 2012 10:51:54 +0200
To: <dest@destdomain.net>
Cc: <dest@destdomain.net>
Subject: le sujet du mail
User-Agent: Heirloom mailx 12.4 7/29/08
Mime-Version: 1.0
Content-Type: multipart/alternative; boundary="----=_Part_3927_12044027.1214951458678"
Content-Transfer-Encoding: 7bit
X-Spam-Status: score=3.7 tests=DNS_FROM_RFC_POST, HTML_00_10, HTML_MESSAGE, HTML_SHORT_LENGTH version=3.1.7
X-Spam-Level: ***
From: Me <from@fromdomain.net>
Status: RO
```

__Le corps :__ Du texte stocké et transporté au format US-ASCII 7bit et pouvant contenir un format plus complexe (mail html)

__Des pièces jointes :__ les pièces jointes sont convertie en chaîne alphanumérique et concaténée au mails.

## Fonctionnement

L'expéditeur utilise le protocole SMTP vers sont serveur SMTP relay ou directement vers le serveur SMTP du destinataire.

Pour faire parvenir le mail au serveur destinataire on récupère au travers du protocole DNS l'entrée MX (mail eXchange) du domaine puis via SMTP on se connect au serveur MX pour transferer le mail

![smtp](./images/smtp.png)

### Les composants logiciels

__MUA :__  Mail User Agent, Le client de messagerie, permet d'émetre et de gèrer les messages
En génral inclu dans le MUA :

- MRA : Mail Retrieval Agent : il permet de consulter une boite mail distante via les protocole POP, IMAP, etc...
- MSA : Mail Submission Agent : il permet d'envoyer les mail vers un MTA

__MTA :__ le Mail Transfert Agent : Le serveur SMTP, gestion des files de mails (mailqueue)

__MDA :__ le Mail Delivery Agent : Il délivre localement le mail (LMTP), dans la boite mail client. Il gère le stockage et les quota de mail.

Les serveurs POP ou IMAP : les Serveur de consultation des boites mails utilisé par les client mail

### Les boites mails

Chaque unité de stockage est une boites mails, mailbox en anglais.

Deux formats :

- mbox : un simple fichier texte contenant tout les mails  
- maildir : une sous arborescence contenant un fichier par mail.

## Les MTA

L'envois est direct ou indirecte via l'utilisation d'un relayhost ou via le traitement de la distribution smtp directe.

Les mails sont filtrés par ip source (seul certain hosts peu utilise ce MTA) ou par destination (il n'accepte que certains domaine de destination)

le MTA peu modifier le nom de domaine en cas d'une émission directe (masquerading : pour ne pas utiliser le hostname en tant que nom de domaine, gestion du champs From)

Exemple de configuration :

- Simple smtp local  : envois les mail vers un relay avec masquerading du domaine mais n'accepte les connexion que localement (sur localhost 127.0.0.1).
- Serveur relay : accepte les connexions du parc uniquement, pour tous domaines de destination, puis effectue une distribution directe
- Serveur de messagerie : accepte les connexions du monde entier mais pour certains domaines uniquement il effectue une distribution (locale ou via un autre relay).

Les 3 en même temps : c'est là que la configuration deviens complexe.

__Les file de traitement (mailqueues) :__ Les serveurs SMTP traitent les mail un par un au travers de file d'atente. L'orsque l'envois d'un message est en échec temporaire (code retour 4XX) l'envois du message est alors différé, remis en file d'attente. Le délais de renvois est sur certain MTA exponentiel (Ex: postfix) à chaque echec temporaire, l'envois est différé à 2 fois le délais précédent.

__Les principaux MTA :__

__Sendmail :__ le standard historique, il supporte de fortes charges mais il est Complexe à configuré. Il a connue de Nombreuses failles de sécurité.

__qmail :__ Ses atouts sont la sécurité et les performances, il est complexe a installer et ne dispose pas de version libre.

__postfix :__ Idem Qmail mais plus simple à gérer, il est intégré aux distributions professionel er deviens le standard

__Exim :__ Légé, facile à mettre en oeuvre et administrer pour des configurations simple. Il est sous licence GNU

### Postfix

C'est un MTA trés réputé et courant. Il est sous licence IBM Public licence (incompatible avec la GPL)

[Documentation riche claire et traduite en français]( http://x.guimard.free.fr/postfix/)

Service mutli processus, modulaire (certain processus peuvent être remplacés ou ajouter) qui maintiens la compatibilité avec sendmail

#### Les processus

- Master : le superviseur, il gère les autres processus
- qmgr : le gestionaire de queues, il distribue les mails pour traitement et distribution
- Pickup : lit les messages d'origine local (compatibilité avec la cmd sendmail)
- smtpd : receptionne les mails via écoute réseau smtp
- cleanup : termine la réception du message (maj header, rewrite)
- trivial-rewrite : réécriture d'adresse mail (pour pickup) et résolution d'adresse de destination (pour qmgr)
- smtp : envois de message via smtp
- local : (mda) distribution local (peut être remplacé par procmail)
- virtual : (mda) distribution vers des comptes de messageries de plusieurs domaines (users virtuels)

#### Les files de traitements

maildrop : file de compatibilité sendmail cette file est suivi par le processus pickup
incoming : file des nouveau mail (sortie de cleanup)
active : la file des mails en cours de traitements
deferred : la file des mails dont l'envois est repoussé
hold : file de mails retenus (non traité)

#### Les maps

Ce sont des tables de relation clé valeures utilisé par postfix : Les domaines , les adresses mail, les aliases, le domaine canonical, les mails routes ou transport, et redirections (relocated)

le stockage peut être en :

- fichier plat, simples paire clé valeur utilisant éventuelement des regex. Lu séquentiellement la première occurrence corespondante est alors utilisée.
- fichier indexés : dbm, btree ou hash, solution rapide et efficace basé sur un fichier text devant être compilé en fichier binaire indexé.
- sources externes : ldap sql, postfix dépend alors du service secondaire associé.

#### Configuration

`/etc/postfix/main.cf` : la configuration globale du MTA
Configuration de l'intégration local : users, répertoires, etc...

`/etc/postfix/master.cf` : la configuration de l'hyperviseur postfix, il permet de configurer les modules a utiliser : le type de communication, s'il est chrooté, sa fréquence de réveil, le nombre maximum de processus, la commande à passer et ses arguments

Toute la [doc](http://www.postfix.org/documentation.html), exemple de configurations standards, résolution de probleme, etc... l'un des projets open-source les mieux documenté.

## Les MDA

Cet agent peut être inclu avec le MTA (postfix) ou avec le serveur POP/IMAP (dovecot). Son rôle principale est la gestion du stockage des mails entrant.

Il peu aussi gèrè des redirections, des règles de filtrages, des réponses automatique et des procédure automatique de traitement (procmail)

Il est en relation étroite avec le MTA car il termine la livraison du message, il peut rejeter les messages (boite pleine, user inconnu) et le mta doit en être informé.
Le mda connait donc les comptes de messagerie et le quota utilisé.

### Gestion multi domaine

un MDA délivre les mail pour les noms de domaines (localhost, 127.0.0.1, IP, hostname.localdomain, hostname). Ce sont les domaines cannoniques (canonical), a destination des utilisiteur du système. $Les mails pour lui même$. Le MDA est alors local.

Il peu aussi servir la destination de domaines hébergés, on les appelle les domaines virtuels (virtual). il sont a destination de boites mails non utilisé par les comptes utilisateur du host. Les MDA virtuels doivent alors maitriser :

- les comptes de messagerie virtuelles
- leur quota respectif
- ou et comment stocker les messages

### Les principaux MDA :

- procmail : Une référence
- local/virtual : inclu avec Postfix
- maildrop : une alternative à procmail
- dovecot : un MDA inclu dans le serveur POP et IMAP

Le MDA est inclu dans exim et sendmail pour la livraison local (domaine cannoniques , pour les compte unix)

#### dovecot

Dovecot est à la fois un MDA et un serveur pop et imap, il s'intègre trés bien avec postfix.

Il sera configuré dans le TD join pour stocker les mail dans `/var/vmail/$domaine` et il utilisera la même base de données que postfix.

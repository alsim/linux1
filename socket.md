# Notion de sockets

## Présentation

Un socket (communément "une prise" en anglais) est un composant logiciel utilisé pour les communication interprocessus (Inter Process Communication IPC)

Au niveau réseaux, le socket fait le lien entre les couches matérielles et les couches applicatives, il parmet l'accès à l'application.

![](./images/socket.png)

Les sockets sont forcément attachés à un processus applicatif. Si on arrête l'application, le socket est alors fermé ou détruit.

## Le support du socket

### socket réseaux

Les sockets de type stream (pour tcp) ou datagram (pour udp) ou encore raw (les packets réseau bruts, seul root peu utiliser les sockets raw).

examples:

* tcp 0.0.0.0:80 : le port tcp 80 (http) sur toute les interface réseaux du host relier au serveur web
* udp 127.0.0.1:53 : le port udp 53 (dns) sur l'interface de loopback relier au server dns local.

En principe :

- L'application au démarrage demande l'ouverture du socket au noyau.
- Le noyau du système, le kernel, gére les couches matérielles et tcp ou udp du modèle OSI puis transmet la sortie de ces couches vers l'application.
- L'application receptionne, traite le flux de données et réponds à ce flux au travers du même socket

> Une connexion tcp est réprésenté par les deux sockets réseaux conectés : adresse ip source:port source, adresse ip destination:port destination

### socket fichier

Communément appelé les sockets unix (Type de fichier "s")

```bash
$ ls -al /var/lib/mysql/mysql.sock 
srwxrwxrwx. 1 mysql mysql 0 Oct  3 10:21 /var/lib/mysql/mysql.sock
$ sudo systemctl stop mariadb.service 
$ ls -al /var/lib/mysql/mysql.sock 
ls: cannot access /var/lib/mysql/mysql.sock: No such file or directory
```

Ce fichier est créé au démarrage de l'application et permet une comunication locale vers cette application sans passer par les couches réseaux.

## la commande ss 

Pour socket stats, Permet de voir l'état des sockets et des sessions sur le système : 

options usuelles : 

- n : pour numérique (pas de résolution numéro vers noms)
- l : Listen les sockets ouvert en écoute
- p : Process : founi aussi le processus qui écoute ce socket
- t : TCP : liste les sockets ou sessions tcp
- u : UDP : liste les sockets udp
- a : all voir tout les socket (en écoute et les session établies)

Exemple :

- Socket réseaux tcp ouvert à la connexion (LISTEN)

```bash
$ sudo ss -ltpn | grep LISTEN
LISTEN     0      100    127.0.0.1:25                       *:*                   users:(("master",pid=1006,fd=13))
LISTEN     0      50     127.0.0.1:3306                     *:*                   users:(("mysqld",pid=10678,fd=14))
LISTEN     0      128          *:111                      *:*                   users:(("rpcbind",pid=342,fd=8))
LISTEN     0      128    192.168.33.31:80                       *:*                   users:(("httpd",pid=1059,fd=3),("httpd",pid=1058,fd=3),("httpd",pid=1057,fd=3),("httpd",pid=1056,fd=3),("httpd",pid=1055,fd=3),("httpd",pid=694,fd=3))
LISTEN     0      128          *:22                       *:*                   users:(("sshd",pid=696,fd=3))
LISTEN     0      100        ::1:25                      :::*                   users:(("master",pid=1006,fd=14))
LISTEN     0      128         :::111                     :::*                   users:(("rpcbind",pid=342,fd=11))
LISTEN     0      128         :::22                      :::*                   users:(("sshd",pid=696,fd=4))
```

- Socket fichiers de mysql

```bash
$ sudo ss -lxpn | grep mysql
u_str  LISTEN     0      50     /var/lib/mysql/mysql.sock 99455                 * 0                   users:(("mysqld",pid=10678,fd=15))
```

- session tcp en cours (ma connexion ssh):

```bash
# ss -anpt | grep -v LISTEN
State      Recv-Q Send-Q Local Address:Port               Peer Address:Port              
ESTAB      0      0      10.0.2.15:22                 10.0.2.2:52418               users:(("sshd",pid=3143,fd=3),("sshd",pid=3139,fd=3))
```

## En conclusion

En débug et troubleshoot il est indispensable de valider et vérifier comment les services réseaux sont accessible, la commande `ss` fait donc partie des commandes à connaitre.

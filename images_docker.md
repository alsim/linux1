# Gestion des images docker

## Présentation des images

Les images docker sont formées de plusieurs couches superposées.

Une image de base sur laquels on viens ajouter des composants logiciels, puis des applicatifs et enfin la couche supérieure sera ouverte à l'écriture au container.

![docker-image](./images/docker-image.png)

Une image est nommée de la façon suivante :

**dépot(ou user)** / **image** : **tag**

Le dépot défini le mainteneur de l'image, il peu être omis pour les images maintenue par docker himself (image de la $librairy$).

le nom de l'image est souvent le nom du logiciel qu'elle porte.

Le tag est en général une versions, plus la version du logiciel que la version de l'image qui porte le logiciel.

## Manipulation d'image

__docker pull:__ Cette commande permet de récupèrer des images sur une $registry$ docker. Par dèfaut, c'est le docker hub. Il est possible de créer sa propre $registry$ d'images.

```bash
[vagrant@localhost ~]$ docker pull busybox
Using default tag: latest
latest: Pulling from library/busybox
0669b0daf1fb: Pull complete
Digest: sha256:b26cd013274a657b86e706210ddd5cc1f82f50155791199d29b9e86e935ce135
Status: Downloaded newer image for busybox:latest
docker.io/library/busybox:latest
```

> busybox est une version minimaliste des commandes gnu que l'on retrouve dans beaucoup d'environnement, cela mérite que vous vous y intéressiez.

__docker image ls:__ Cette commande permet de lister les images disponibles localement.

```bash
[vagrant@localhost ~]$ docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
busybox             latest              83aa35aa1c79        7 days ago          1.22MB
alpine              latest              e7d92cdc71fe        8 weeks ago         5.59MB
hello-world         latest              fce289e99eb9        14 months ago       1.84kB
```

__docker image rm:__ cette commende supprime une image localement

```bash
[vagrant@localhost ~]$ docker image rm busybox
Untagged: busybox:latest
Untagged: busybox@sha256:b26cd013274a657b86e706210ddd5cc1f82f50155791199d29b9e86e935ce135
Deleted: sha256:83aa35aa1c79e4b6957e018da6e322bfca92bf3b4696a211b42502543c242d6f
Deleted: sha256:a6d503001157aedc826853f9b67f26d35966221b158bff03849868ae4a821116
```

## Modification d'image

On démarre un container à partir d'une image alpine et le processus sh.

On installe python avec le gestionaire de package apk de alpine

on teste puis on quite le container

```bash
[vagrant@localhost ~]$ docker run -it alpine sh
/ # apk add python
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/community/x86_64/APKINDEX.tar.gz
(1/9) Installing libbz2 (1.0.8-r1)
(2/9) Installing expat (2.2.9-r1)
(3/9) Installing libffi (3.2.1-r6)
(4/9) Installing gdbm (1.13-r1)
(5/9) Installing ncurses-terminfo-base (6.1_p20200118-r2)
(6/9) Installing ncurses-libs (6.1_p20200118-r2)
(7/9) Installing readline (8.0.1-r0)
(8/9) Installing sqlite-libs (3.30.1-r1)
(9/9) Installing python2 (2.7.16-r3)
Executing busybox-1.31.1-r9.trigger
OK: 46 MiB in 23 packages
/ # python
Python 2.7.16 (default, Nov 15 2019, 19:32:34)
[GCC 9.2.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>>
/ # exit
```

Il est maintenant possible de figer cette nouvelle image avec la commande __docker commit__

```bash
[vagrant@localhost ~]$ docker commit 12650b70e9e4 mine/python:2.7.16
sha256:92bc62f5183d61a55ce96ab9915df97527664622938a944aa73c3f643d590b4c
[vagrant@localhost ~]$ docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
mine/python         2.7.16              92bc62f5183d        10 seconds ago      44.1MB
alpine              latest              e7d92cdc71fe        8 weeks ago         5.59MB
hello-world         latest              fce289e99eb9        14 months ago       1.84kB
```

Nous disposons alors d'une nouvelle image : mine/python:2.7.16 que nous pouvons utiliser pour instancier un container python :

```bash
[vagrant@localhost ~]$ python --version
Python 2.7.5
[vagrant@localhost ~]$ docker run -it mine/python:2.7.16 python
Python 2.7.16 (default, Nov 15 2019, 19:32:34)
[GCC 9.2.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>>  print ('\o/')
\o/
```

On dispose alors de python 2.7.16 sur une machine ou il n'est pas installé.

Bon c'est sympa, mais c'est pas comme ça que ça s'utilise en vérité.

## Construction d'image

### Le Dockerfile

le docker file est un fichier permettant de définir les actions de construction d'une image.

Exemple :

```dockerfile
FROM alpine
EXPOSE 5000
ENTRYPOINT ["./run.sh", "inside a container"]
RUN apk add python3 && \
     pip3 install --no-cache --upgrade pip setuptools wheel && \
     pip3 install flask
ADD appli /opt/appli
WORKDIR /opt/appli
USER operator
```

Quelques explications sur les principale option de build :

- FROM $image$ : Défini l'image source qui sert de base au build, il peu arriver de partir de rien mais c'est plutot rare, on ne réinvente pas la roue.
- RUN $cmd$ : Permet de lancer une commande de construction de l'image et commit la couche ainsi réalisée. on réalise des commandes longue afin de réduire le nombre de commit et donc de couche, si une seul des commandes séparrée par des && échoue, l'ensemble du RUN échoue.
- ADD $path$ : Avec on ajoute du contenu présent dans le dossier de build dans le container et on commit la couche.
- WORKDIR $dir$ : défini le dossier de travail à partir de là.
- USER $user$ : defini avec quel uid on travail à partir de là.
- EXPOSE $port$ : Défini le port réseaux qui sera exposé (à titre informatif car cela est fait à l'instanciation du container, on le vera plus loin)
- ENTRYPOINT $list$: Si existant cela défini la commande et les argumenets qui seront effectivement lancée dans le container à l'instanciation.
- CMD $list$ : Fourni soit la commande par défaut du container si entrypoint n'est pas présent), soit les arguments par défaut ajouté à l'entrypoint. Ces valeurs sont surchargées par les arguments précisés dans la commande docker run après l'image.
- VOLUME $list$ : défini un point de montage dans l'image : volume sera monté au moment du run (on le vera plus loin).

> d'autre commandes existent, consultez la [documentation officielle](https://docs.docker.com/engine/reference/builder/)

### Gestion du build

Maintenant que l'on sais faire un dockerfile on peu contruire des images avec la commandes `docker build`, on précisera :

- Le nom du container avec l'option -t (tag) c'est bien plus pratique qu'un hash.
- Eventuelement le nom du docker file avec l'option -f sinon par défaut c'est **Dockerfile**
- Le dossier de build (car les ajouts de fichier sont fait en chemin relatifs depuis ce dossier) sinon c'est le dossier courant

Avec le Dockerfile suivant dans le dossier courant:

```docker
FROM alpine
ENTRYPOINT ["python"]
ENV PYTHONUNBUFFERED=1
RUN apk add --no-cache python && \
    python -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip install --upgrade pip setuptools && \
    rm -r /root/.cache
```

```bash
[vagrant@localhost tmp]$ docker build -t mine/appli:latest .
.../...
```

Il faudra reconstruire l'image à chaque modification de ce qui le constitue.

Si on le fait de suite, sans changement le build est beaucoup plus rapide :

```bash
[vagrant@localhost tmp]$ docker build -t mine/appli:latest .
Sending build context to Docker daemon  4.096kB
Step 1/7 : FROM alpine
 ---> e7d92cdc71fe
Step 2/7 : RUN apk add python3 &&      ln -sf python3 /usr/bin/python &&      pip3 install --no-cache --upgrade pip setuptools wheel &&      pip3 install flask
 ---> Using cache
 ---> 51ffd4b8e961
Step 3/7 : ADD appli /opt/appli
 ---> Using cache
 ---> 383e94bff732
Step 4/7 : WORKDIR /opt/appli
 ---> Using cache
 ---> d96439110d2d
Step 5/7 : EXPOSE 1080
 ---> Using cache
 ---> c8f6049e7c2b
Step 6/7 : USER operator
 ---> Using cache
 ---> 8d91e82d5ad6
Step 7/7 : ENTRYPOINT ["./run.sh", "indocker"]
 ---> Using cache
 ---> 25f4fb4fce61
Successfully built 25f4fb4fce61
Successfully tagged mine/appli:latest
[vagrant@localhost tmp]$
```

Chaque étape, chaque commit, pour chacune des couches, le résultat est récupèré en cache et n'est donc pas reconstruit. Si en revanche, un étape est modifiée celle-ci est reconstruite et les suivantes aussi.

## En conclusion

Nous savons donc maintnenat manipuler et contruire les images nous permettant ensuite d'utiliser des containers.

> construisez une image perl:5 qui exécute systématiquement la commande suivante et ne fais rien d'autre :
>  
>  ```sh
>  perl -e 'print("hello world\n")'
>  ```

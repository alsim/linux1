# étude d'un package debian

On se créé un rep de travail

```bash
mkdir work ; cd work
```

On récupère un package

```bash
wget http://ftp.fr.debian.org/debian/pool/main/a/at/at_3.1.13-2+deb7u1_amd64.deb 
```

On extrait les fichier de l'archive ar (du package)

```bash
ar -x at_3.1.13-2+deb7u1_amd64.deb
```

On se créé un sous rep extrait les fichiers control

```bash
mkdir control ; cd control/
tar zvxf ../control.tar.gz 
```

On osculte les fichiers.

On décompresse l'archive et on osculte

```bash
unxz data.tar.xz
tar tvf data.tar
```
# Installer correctement Docker :whale: avec WSL

Installer Docker sur Windows c'est une **épopée**, il faut faire la guerre à son OS parce que l'outil `Docker Desktop` n'est pas compatible avec nos versions `Familiale`.

Par contre, si vous avez une version `Pro` ou `Enterprise` c'est un jeu d'enfant. Il suffit d'installer l'outil ici : https://hub.docker.com/editions/community/docker-ce-desktop-windows

## Comment faire ?

Docker a developpé un outil pour les PCs ne remplissant pas les conditions requises (on aura juste pas de GUI mais on s'en fou un peu)

Veuillez vous rendre sur ce lien et installer le .exe : https://github.com/docker/toolbox/releases

Installez la toolbox. Veuillez décocher `Virtualbox` quand ça vous est proposé (Vous l'avez déjà normalement, et avec une bien meilleure version.)

Une fois ceci fait, ouvrez un CLI Windows et tapez ces commande :

```
docker-machine create -d virtualbox default
docker-machine.exe start default
docker-machine.exe ip
```

Si votre machine affiche l'IP c'est bon !

En cas d'erreur :

```
open C:\Users\VOTREUSER\.docker\machine\machines\default\config.json: The system cannot find the file specified.
```

Supprimez manuellement le dossier `C:\Users\VOTREUSER\.docker\machine\machines` et retentez.

Ensuite, tapez

```
docker-machine env
```

Vous allez obtenir quelque chose d'extrêmement semblable :

```
C:\Users\VOTREUSER>docker-machine env
SET DOCKER_TLS_VERIFY=1
SET DOCKER_HOST=tcp://192.168.99.101:2376
SET DOCKER_CERT_PATH=C:\Users\VOTREUSER\.docker\machine\machines\default
SET DOCKER_MACHINE_NAME=default
SET COMPOSE_CONVERT_WINDOWS_PATHS=true
REM Run this command to configure your shell:
REM     @FOR /f "tokens=*" %i IN ('docker-machine env') DO @%i
```

Executez ces commandes ligne par ligne dans votre CLI.

Attention : pour le `@FOR` il faut enlever le `REM` devant.

Pour tester que tout fonctionne correctement :

```
docker run hello-world
```

:fire: :fire: Votre Docker fonctionne sur Windows ! :fire: :fire:

## Faire fonctionner Docker :whale: sur WSL

Avant de commencer, afin de faire fonctionner les volumes docker, vous devez monter le host sur /c dans WSL (par défaut il est monté sur /mnt/c) : rajoutez ces deux lignes sous `[automount]` dans `/etc/wsl.conf`

```
root = /
enabled = true
```

Dans un CLI Windows :

```
wsl --terminate ubuntu
wsl
```

Installez docker-ce et docker-compose via le site internet de Docker

CF : https://docs.docker.com/install/linux/docker-ce/ubuntu/
https://docs.docker.com/compose/install/

Mettez ensuite ceci dans votre .bashrc :

```
# Docker (Docker Toolbox on Windows)
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://192.168.99.101:2376"
export DOCKER_CERT_PATH="/c/Users/VOTREUSER/.docker/machine/machines/default"
export DOCKER_MACHINE_NAME="default"
export COMPOSE_CONVERT_WINDOWS_PATHS="true"

alias docker-machine="docker-machine.exe"
docker-machine.exe start default && # autostart docker-machine
export COMPOSE_CONVERT_WINDOWS_PATHS=1
```

Et enfin, il faut créer des liens symboliques pour les clés :

```
mkdir -p ~/.docker
ln -s /c/Users/VOTREUSER/.docker/machine/certs/ca.pem ~/.docker/ca.pem
ln -s /c/Users/VOTREUSER/.docker/machine/certs/ca-key.pem ~/.docker/ca-key.pem
ln -s /c/Users/VOTREUSER/.docker/machine/certs/cert.pem ~/.docker/cert.pem
ln -s /c/Users/VOTREUSER/.docker/machine/certs/key.pem ~/.docker/key.pem
```

Vous devriez être capable de lancer des containers directement depuis WSL :fire:
